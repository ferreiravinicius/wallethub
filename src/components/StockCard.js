import React from 'react';
import { Card, CardHeader } from '@material-ui/core';

const StockCard = ({ stock }) => {

  const { name, symbol } = stock;

  return (
    <Card>
      <CardHeader title={symbol} subheader={name} />
    </Card>
  )
}

export default StockCard;