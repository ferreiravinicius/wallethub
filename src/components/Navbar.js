import React from 'react';
import StyledNavbar from '../styles/StyledNavbar';


const Navbar = ({ children }) => {

  const renderChildren = (children) => {
  
    let isMultipleChindren = Array.isArray(children);
  
    if (isMultipleChindren) {
      return children.map((child, idx) => {
        return (
          <li key={idx}>
            {child}
          </li>
        );
      })
    }
  
    return <li>{children}</li>;
  }

  return (
    <StyledNavbar>
      {children &&
        <ul>
          {renderChildren(children)}
        </ul>
      }
    </StyledNavbar>
  );
}

export default Navbar;