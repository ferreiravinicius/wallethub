import React from 'react';
import StyledHeader from '../styles/StyledHeader';
import Flex from '../styles/Flex';
import Link from '../styles/Link';
import Container from '../styles/Container';
import Navbar from './Navbar';

const Header = () => {
  return (  
    <StyledHeader>
      <Container>
            <Flex justifyContent="space-between" alignItems="center">
              <section className="logo">
                <img src="https://peoplepng.com/wp-content/uploads/2019/03/generic-company-logo-png-7.png" alt="logo" />
              </section>
              <Navbar>
                <Link href="https://www.facebook.com/ferreiravinicius" blank iconClass="fab fa-facebook" />
                <Link href="https://www.github.com/ferreiravinicius" blank iconClass="fab fa-github-alt" />
              </Navbar>
            </Flex>
          </Container>
    </StyledHeader>
  );
}
 
export default Header;