import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import MockedStockAPI from '../apis/MockedStockAPI'
import StockCard from './StockCard';
import Flex from '../styles/Flex';

const StockManager = () => {

  const [allStocks, setAllStocks] = useState([]);
  const [userId, setUserId] = useState(0);

  useEffect(() => {
    let canceled = false;

    async function fetch() {
      const listAllStocks = await MockedStockAPI.findAll(userId);
      if (!canceled)
        setAllStocks(listAllStocks);
    };

    fetch();
    return () => canceled = true;
  }, [userId]);

  return ( 
    <>
      {allStocks && allStocks.length > 0 ?
          
        <Grid container spacing={1}>
          {allStocks.map(stock => {
            return (
              <Grid key={stock.symbol} item xs={6}>
                <StockCard stock={stock} />
              </Grid>
            )
          })}
        </Grid>
      :
        <></>
      }

    </>
  );
}
 
export default StockManager;