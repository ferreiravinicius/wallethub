
const MockedStockAPI = {}

MockedStockAPI.findAll = async (userId) => {
  return [
    {symbol: "BIDI4", name: "Banco Inter SA"},
    {symbol: "PETR4", name: "Petrobrás SA"},
    {symbol: "MGLU3", name: "Magazine Luiza Ltda."},
    {symbol: "LAME4", name: "Lojas Americanas Ltda."}
  ]
}

export default MockedStockAPI;