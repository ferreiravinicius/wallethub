import axios from 'axios';
import util from 'lodash';

const API_KEY = 'NWZJHLTMZVD59CNL';
const ENTRY_SYMBOL = "1. symbol";
const ENTRY_NAME = "2. name";
const ENTRY_TYPE = "3. type";
const ENTRY_REGION = "4. region";
const EQUITY = "Equity";
const BRASIL = "Brazil/Sao Paolo";


const AlphavantageAPI = {
  firstMatch: _firstMatch,
  allMatches: _allMatches
}

async function _allMatches(query) {
  try {

    if (util.isEmpty(util.trim(query)))
      return [];

    const uri = `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${query}&apikey=${API_KEY}`;
    let response = await axios.get(uri);
    return response.data.bestMatches.filter(item => item[ENTRY_TYPE] === EQUITY && item[ENTRY_REGION] === BRASIL).map(item => _convert(item));
  } catch (error) {
    console.log(error);
  }
}

async function _firstMatch(symbol) {
  try {
    const uri = `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${symbol}&apikey=${API_KEY}`;
    let response = await axios.get(uri);
    console.log(response)
    let matched = util.first(response.data.bestMatches)
    return matched ? _convert(matched) : null;
  } catch (error) {
    console.log(error);
  }
}

function _convert(stock) {
  return { name : stock[ENTRY_NAME] , symbol : stock[ENTRY_SYMBOL] };
}

export default AlphavantageAPI;

