import { CssBaseline } from '@material-ui/core';
import React from 'react';
import App from './App';

const AppWrapper = () => {
  return (
    <>
      {/* Normalize em componente para todo css */}
      <CssBaseline />
      {/* Inicio da aplicação */}
      <App />
    </>
  );
}

export default AppWrapper;