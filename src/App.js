import React from 'react';
import styled from '@material-ui/styles/withStyles';
import { width } from '@material-ui/system';
import { Container, Grid } from '@material-ui/core';

const styles = theme => ({
  nope: {
    border: '2px solid red'
  }
});

const App = ({classes}) => {
  return (
    <>
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} className={classes.nope}>Yay</Grid>
          <Grid item xs={12} sm={6} className={classes.nope}>Yay</Grid>
          <Grid item xs={12} sm={6} className={classes.nope}>Yay</Grid>
          <Grid item xs={12} sm={6} className={classes.nope}>Yay</Grid>
        </Grid>
      </Container>
    </>
  );
}

export default styled(styles)(App);
