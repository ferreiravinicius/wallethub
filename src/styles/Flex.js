import styled from 'styled-components';

export default styled.section`
  display: flex;
  flex-wrap: ${props => props.wrap || "nowrap"};
  flex-basis: ${props => props.basis || "auto"};
  flex-direction: ${props => props.direction || "row"}; /*row|row-reverse|column|column-reverse|initial|inherit;*/
  justify-content: ${props => props.justifyContent || "flex-start"}; /* flex-start|flex-end|center|space-between|space-around|initial */
  align-items: ${props => props.alignItems || "stretch"}; /* stretch|center|flex-start|flex-end|baseline|initial */
`;