import React from 'react';
import styled from 'styled-components';
import { Icon } from '@material-ui/core';

const StyledLink = styled.a`
  color: blue;

  :hover {
    color: yellow;
  }
`;

const Link = ({ children, href, blank, iconClass }) => {
  return (
    <StyledLink href={href ? href : "#"} target={blank ? '_blank' : '_self'}>
      {iconClass &&
        <Icon className={iconClass} />
      }
      {children}
    </StyledLink>
  );
}

export default Link;