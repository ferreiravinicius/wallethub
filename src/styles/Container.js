import styled from 'styled-components';

export default styled.section`
  max-width: 1024px;
  margin: 0 auto;
`;