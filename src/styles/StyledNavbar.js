import styled from 'styled-components';

export default styled.nav`
  ul {
    list-style-type: none;
    display: flex;
    padding: 0;
    margin: 0;

    li {
      margin-left: .5em;
      padding: .5em;
      cursor: pointer;
    }
  }
`;