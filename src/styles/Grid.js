import styled from 'styled-components';

const template = (props) => {

  const { templateAreas, numColumns, numRows } = props || {};

  if (templateAreas)
    return `grid-template-areas: ${templateAreas}`;

  return `
    grid-template-columns: repeat(${numColumns || 1}, 1fr);
    grid-template-rows: repeat(${numRows || 1}, 1fr);
  `;
}

const gap = (props) => {

  const { gap, columnGap, rowGap } = props || {};

  if (gap)
    return `grid-gap: ${gap}`;

  return `
    grid-column-gap: ${columnGap || 0};
    grid-row-gap: ${rowGap || 0};
  `;
}

export default styled.section`
  display: grid;
  ${props => gap(props)}
  ${props => template(props)}
`;