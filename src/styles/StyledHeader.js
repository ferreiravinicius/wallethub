import styled from 'styled-components';

export default styled.header`
  justify-content: space-between;
  background-color: yellow;
  padding: .2em 0;
  width: 100vw;

  .logo {
    height: 40px;

    img {
      display: block;
      height: 100%;
    }
  }

  nav, .menu {
    ul {
      list-style-type: none;
      display: flex;
      padding: 0;
      margin: 0;

      li {
        margin-left: .5em;
        padding: .5em;
        cursor: pointer;
      }
    }
  }
`;